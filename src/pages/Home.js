// dependencies
import React from 'react'

//Bootstrap dependencies
import Container from 'react-bootstrap/Container';

//App Componenets

import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import Course from '../components/CourseCard.js';

export default function Home(){
	return(
		<Container fluid>
			<Banner/>
			<Highlights/>
			<Course/>
		</Container>
		)
}
