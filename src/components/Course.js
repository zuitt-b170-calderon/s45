// dependencies
import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';

import Button from 'react-bootstrap/Button';

export default function Course(props0){
	let course = props.course;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);

	function enroll(){
		setCount (count +1)
	}
	function seatCount(){
		if (seats > 0){
			setCount (seats -1)
		}else {
			alert ('No more seats.')
		}
	}
	return (
		
			<Card>
				<Card.Body>
					<Card.Title>Sample Course</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>
						This is a sample course offering.
					</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php 40,000</Card.Text>
					<Button>Enroll</Button>
				</Card.Body>
			</Card>
			
		)
}

