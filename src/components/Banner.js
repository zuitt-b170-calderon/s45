/*
import the following:
	React from react

	Jumbotron from react-bootstrap/Jumbotron
	Button from react-bootstrap/Button
	Row from react-bootstrap/Row
	Col from react-bootstrap/Col
*/
//Dependencies
import React from 'react';


import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner(){
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for Everyone, everywhere.</p>
					<Button variant="primary">Enroll Now!</Button>
				</Jumbotron>
			</Col>
		</Row>				
		)
}